using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Moq;
using Microsoft.VisualBasic;


namespace AVPTesten
{
    public class AVPTest
    {

        private Mock<Vehicle> moqCar()
        {
            var v = new Mock<Vehicle>(MockBehavior.Default, 0, 0, 0);
            v.Setup(vehicle => vehicle.ValueInEuros).Returns(25000);
            v.Setup(vehicle => vehicle.Age).Returns(6);
            v.Setup(vehicle => vehicle.PowerInKw).Returns(50);
            return v;
        }


        private Mock<PolicyHolder> moqPolicy(int licenseAge = 25, int age = 45, int postalCode = 7788, int noClaimYears = 0)
        {
            var mPH = new Mock<PolicyHolder>(MockBehavior.Default, 0, "1-1-2021", 0, 0);
            mPH.Setup(holder => holder.LicenseAge).Returns(licenseAge);
            mPH.Setup(holder => holder.NoClaimYears).Returns(noClaimYears);
            mPH.Setup(holder => holder.PostalCode).Returns(postalCode);
            mPH.Setup(holder => holder.Age).Returns(age);
            return mPH;
        }

        [Theory]
        [InlineData(15, 22, 1.15)]
        [InlineData(15, 23, 1.00)]
        [InlineData(15, 24, 1.00)]
        [InlineData(4, 35, 1.15)]
        [InlineData(5, 35, 1.00)]
        [InlineData(6, 35, 1.00)]
    
        public void PremiumCalculationTest_Under23_orShorterthan5yearsDriverLicense(int licenseAge, int age, double expected)
        {
            //Arrange
            var moqCars= moqCar();
            var moqPH = moqPolicy(licenseAge: licenseAge, age: age);
            var standard = 84.67;

            //Act
            var premiumCalculation = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA);

            var second = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(second / standard, 2));
        }


        [Theory]
        [InlineData(3400, 1.05)]
        [InlineData(3500, 1.05)]
        [InlineData(3600, 1.02)]
        [InlineData(4400, 1.02)]
        [InlineData(4500, 1)]
        public void PremiumCalculationTest_PostcodeSave(int postalCode, double expected)
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH = moqPolicy(postalCode: postalCode);
            var standard = 84.67;

            //Act
            var premiumCalculation = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA);

            var second = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(second / standard, 2));
        }

        [Fact]
        public void PremiumCalculationTest_premiereroundedto2digits()
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH = moqPolicy();

            //Act
            var premiumPaymentAmount = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.True(premiumPaymentAmount * 100 % 1 == 0);
        }

        [Fact]
        public void PremiumCalculationTest_MonthCalculate()
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH = moqPolicy();
            var expectedMonth = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR) / 12;

            //Act
            var premiumPaymentAmountMonth = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(Math.Round(expectedMonth), Math.Round(premiumPaymentAmountMonth));
        }
        [Fact]
        public void PremiumCalculationTest_DiscountYearPayment()
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH = moqPolicy();
            var expected = 85;

            //Act
            var premiumPaymentAmount = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.Equal(expected, Math.Round(premiumPaymentAmount * 1.025));
        }

        [Theory]
        [InlineData(5, 1.00)]
        [InlineData(10, 0.75)]
        [InlineData(15, 0.50)]
      
        public void PremiumCalculationTest_NoDamageYearDiscount(int noClaimYears, double expected)
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH = moqPolicy(noClaimYears: noClaimYears);
            var standard = 84.67;

            //Act
            var premiumCalculation = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA);

            var second = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(second / standard, 2));
        }

        [Fact]
        public void PremiumCalculationTest_CalculateWorthWithoutSale()
        {
            //Arrange
            var moqCars = moqCar();
            var expected = 84.67;

            //Act
            var premiumCalculation = PremiumCalculation.CalculateBasePremium(moqCars.Object);

            //Assert
            Assert.Equal(expected, Math.Round(premiumCalculation, 2));
        }

        [Fact]
        public void PremiumCalculationTest_PremiumRaiseWithBetterPremiums()
        {
            //Arrange
            var moqCars = moqCar();
            var moqPH= moqPolicy();
            var standard = 84.67;
            var WAPlusExpect = 1.20;
            var ALLRISKExpect = 2.00;

            //Act
            var ALLRISKPremCalc = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.ALL_RISK);
            var WAPLUSPremiumCalc = new PremiumCalculation(moqCars.Object, moqPH.Object, InsuranceCoverage.WA_PLUS);

            var WAPLUSPrem = Math.Round(WAPLUSPremiumCalc.PremiumAmountPerYear, 2);
            var ALLRISKPrem = Math.Round(ALLRISKPremCalc.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(WAPlusExpect, Math.Round(WAPLUSPrem / standard, 2));
            Assert.Equal(ALLRISKExpect, Math.Round(ALLRISKPrem / standard,2));
        }




    }
}
