AVP Tests

Woord vooraf:

Ik heb gekozen om mijn functies in het Engels te benoemen omdat ik dat ten eerste beter vind staan en ten tweede kan iedereen
de test gebruiken en begrijpen.

##PremiumCalculationTest_Under23_orShorterthan5yearsDriverLicense

Doelstelling:De premie moet met 15% verhoogt worden bij een leeftijd onder 23 of bij een rijbewijs leeftijd korter dan 5 jaar.

De data die ik heb gebruik zijn waardes van het rijbewijs en leeftijd, plus een 1 extra waarden erbij.

De techniek die ik heb gebruik zijn: Theory and Mock.
Theory omdat er meerdere waardes gecheckt moeten worden
En Mock om zo lang mogelijk nog gebruik te maken van de test

Fouten: 
Er staat een fout met een kleiner dan teken, dan moet aangepast worden
Mock werkt niet, 100%. Je moet public int velden op virtual zetten.


##PremiumCalculationTest_PostcodeSave

Doelstelling: Bij een postcode van 10xx t/m 35xx moet de risico opslag 5% zijn en bij 36xx t/m 44xx moet het 2%. 
Daarnaast moet het niet verhogen bij een andere getal.

De data die ik heb gebruik zijn waardes van postcodes. 

De techniek die ik heb gebruik zijn: Theory and Mock.
Theory omdat er meerdere waardes gecheckt moeten worden
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:
Niet van toepassing 


##PremiumCalculationTest_premiereroundedto2digits

Doelstelling: De premie moet met 2 cijfers afgerond worden.

De data is niet van toepassing 

De techniek die ik heb gebruik zijn: Fact and Mock.
Fact voor eenmalig gebruik. 
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:
Niet van toepassing 

##PremiumCalculationTest_MonthCalculate

Doelstelling: Van jaarpremie naar maandpremie

De data is niet van toepassing 

De techniek die ik heb gebruik zijn: Fact and Mock. 
Fact voor eenmalig gebruik. 
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:
Niet van toepassing 


##PremiumCalculationTest_DiscountYearPayment

Doelstelling: Jaar korting test 

De data is niet van toepassing 

De techniek die ik heb gebruik zijn: Fact and Mock. 
Fact voor eenmalig gebruik. 
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:
Niet van toepassing 

##PremiumCalculationTest_NoDamageYearDiscount 

Doelstelling: Test om te kijken of de premies wel verhoogd worden 

De data die ik heb gebruik zijn waardes van schadevrije jaren. 

De techniek die ik heb gebruik zijn: Theory and Mock. 
Theory omdat er meerdere waardes gecheckt moeten worden
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:

Een int waarde bij een functie blokkeerden de berekening. 


##PremiumCalculationTest_CalculateWorthWithoutSale

Doelstelling: Kijken of de basis premie wel goed is doormiddel van een berekening 

De data is niet van toepassing 

De techniek die ik heb gebruik zijn: Fact and Mock. 
Fact voor eenmalig gebruik. 
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten:

Pakte mijn eigen berekening niet, wat 84.67 is. 
Mock werkt niet, 100%. Je moet public int velden op virtual zetten.


##PremiumCalculationTest_PremiumRaiseWithBetterPremiums

Doelstelling: Checken of Wa plus 20% duurders en kijken of All risk dubbele premie heeft

De data is WA Plus en ALL Risk

De techniek die ik heb gebruik zijn: Fact and Mock. 
Fact voor eenmalig gebruik. 
En Mock om zo lang mogelijk nog gebruik te maken van de test 

Fouten: 

Niet van toepassing